export default {
  locales: [
    {
      code: 'vi',
      iso: 'vi-VN',
      name: 'Vietnamese',
      file: 'vi.json'
    },
    {
      code: 'en',
      iso: 'en-US',
      name: 'English',
      file: 'en-US.json'
    }
  ],
  defaultLocale: 'vi',
  detectBrowserLanguage: {
    useCookie: true,
    cookieKey: 'my_site_cookie_locale',
    alwaysRedirect: true,
    onlyOnRoot: true
    // useCookie: false,
    // cookieCrossOrigin: true,
    // cookieKey: 'my_custom_cookie_name'
  },
  baseUrl: '',
  seo: true,
  lazy: true,
  parsePages: false,
  vueI18n: {
    fallbackLocale: ['en', 'vi']
  },
  langDir: 'locales/',
  encodePaths: false
}